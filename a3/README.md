> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Heather Kwak

### Assignment 3:



#### README.md file should include the following items:

1. Screenshot of **your** SQL code;
2. Screenshot of **your** populated tables (w/in the **Oracle environment**);
3. Optional: SQL code for the required reports.
4. Bitbucket repo links: **Your** lis3781 Bitbucket repo link





#### Assignment Screenshots:

*Screenshot of A3 SQL Code 1:*

![A3 SQL Code 1](img/a3_sql_code1.PNG)

*Screenshot of A3 SQL Code 2:*

![A3 SQL Code 2](img/a3_sql_code2.PNG)

*Screenshot of A3 SQL Code 3:*

![A3 SQL Code 3](img/a3_sql_code3.PNG)

*Screenshot of A3 SQL Code 4:*

![A3 SQL Code 4](img/a3_sql_code4.PNG)

*Populated Tables within the **Oracle Environment***

![Populated Tables](img/populated_tables.PNG)

*SQL code for the required reports.*

![SQL code for the required reports](img/a3_sql_report1.PNG)

![SQL code for the required reports](img/a3_sql_report2.PNG)