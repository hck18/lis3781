> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Heather Kwak

### Assignment 4:



#### README.md file should include the following items:

1. Screenshot of *your* ERD; 
2. Optional: SQL code for the required reports. 
3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link



#### Assignment Screenshots:

*Screenshot my ERD:*

![ERD Screenshot](img/a4_erd.png)

