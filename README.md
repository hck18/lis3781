> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Management

## Heather Kwak

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Screenshots of SQL Code and Populated Tables
3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Screenshots of SQL Code and Populated Tables in Oracle
4. [A4 README.md](a4/README.md "My A4 README.md file")
    * Screenshot of *your* ERD;
    * Optional: SQL code for the required reports.
5. [A5 README.md](a5/README.md "My A5 README.md file")
    * Screenshot of *your* ERD;
    * Optional: SQL code for the required reports.
6. [P1_README.md](p1/README.md "My P1 README.md file")
    * Screenshot of *your* ERD;
    * Optional: SQL code for the required reports.
7. [P2 README.md](p2/README.md "My P2 README.md file")
    * Screenshot of at least one MongoDB shell command(s), (e.g., show collections); 
    * Optional: JSON code for the required reports.