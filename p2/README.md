# LIS3781 - Advanced Database Management

## Heather Kwak

### Project 2:



#### README.md file should include the following items:

1. Screenshot of at least one MongoDB shell command(s), (e.g., show collections); 
2. Optional: JSON code for the required reports. 
3. Bitbucket repo links: *Your* lis3781 Bitbucket repo link





#### Project 2 Screenshots:

Screenshot of MongoDB shell command

![MongoDB shell command Screenshot](img/mongodb_shell_command1.png)

JSON Code for required report:

![JSON Code Screenshot](img/json_code1.png)

![JSON Code Screenshot](img/json_code2.png)