> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Heather Kwak

### Assignment 2:



#### README.md file should include the following items:

1. Screenshot of *your* SQL code; 

2. Screenshot of *your* populated tables;



#### Assignment Screenshots:

*Screenshot of A2 SQL Code 1:*

![A2 SQL Code 1](img/a2_sql_code1.png)

*Screenshot of A2 SQL Code 2:*

![A2 SQL Code 2](img/a2_sql_code2.png)

*Screenshot of A2 SQL Code 3:*

![A2 SQL Code 3](img/a2_sql_code3.png)

*Populated Tables*

![Populated Tables](img/populated_tables.png)